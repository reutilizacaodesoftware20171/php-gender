<?php 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Headers: Content-Type"); ?> <?php

require_once('settings.php');

$query = $_GET["query"];
$tweets = getTweetsWith($query);

$gender = array();
foreach ($tweets as &$tweet) {
	$result = $DatumboxAPI->GenderDetection($tweet);
	if (!isset($gender[$result])) {
	    $gender[$result] = 0;
	}
	$gender[$result]++;
}

unset($DatumboxAPI);

print json_encode($gender);